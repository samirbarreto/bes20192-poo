package br.ucsal.bes20192.poo.atividade01;

import java.util.Scanner;

/*
 3. Suponha que o conceito de um aluno seja determinado em fun��o da sua nota. Suponha,
tamb�m, que esta nota seja um valor inteiro na faixa de 0 a 100, conforme a seguinte faixa:

Nota Conceito
0 a 49		Insuficiente
50 a 64		Regular
65 a 84		Bom
85 a 100 	�timo

Crie um programa em Java que leia a nota de um aluno e apresente o conceito do mesmo.

 */
public class Questao03 {

	private static final String CONCEITO_INSUFICIENTE = "insuficiente";
	private static final String CONCEITO_REGULAR = "regular";
	private static final String CONCEITO_BOM = "bom";
	private static final String CONCEITO_OTIMO = "otimo";

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int nota;
		String conceito;

		nota = obterNota();
		conceito = calcularConceito(nota);
		exibirConceito(conceito);
	}

	private static void exibirConceito(String conceito) {
		System.out.println("Conceito = " + conceito);
	}

	private static String calcularConceito(int nota) {
		String conceito;
		if (nota >= 0 && nota <= 49) {
			conceito = CONCEITO_INSUFICIENTE;
		} else if (nota >= 50 && nota <= 64) {
			conceito = CONCEITO_REGULAR;
		} else if (nota >= 65 && nota <= 84) {
			conceito = CONCEITO_BOM;
		} else if (nota >= 85 && nota <= 100) {
			conceito = CONCEITO_OTIMO;
		} else {
			conceito = "nota invalida";
		}
		return conceito;
	}

	private static int obterNota() {
		int nota;
		System.out.println("Digite sua nota:");
		nota = sc.nextInt();
		return nota;
	}

}
