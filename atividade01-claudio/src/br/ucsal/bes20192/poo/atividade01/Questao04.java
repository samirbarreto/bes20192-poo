package br.ucsal.bes20192.poo.atividade01;

import java.util.Scanner;

/*
 4. Crie um programa em Java para ler uma letra do alfabeto e mostrar uma mensagem: se �
vogal ou consoante.
 */
public class Questao04 {
	
	private static final String VOGAIS = "AEIOU";
	
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		String letra;
		Boolean isVogal;
		
		letra = obterLetra();
		isVogal = verificarSeVogal(letra);
		exibirTipoLetra(letra, isVogal);
	}

	private static void exibirTipoLetra(String letra, Boolean isVogal) {
		if (isVogal) {
			System.out.println("A letra --" + letra + "-- � um vogal");
		} else {
			System.out.println("A letra --" + letra + "-- � um consoante");
		}
	}

	private static Boolean verificarSeVogal(String letra) {
		Boolean isVogal;

		if (VOGAIS.indexOf(letra.toUpperCase()) >= 0) {
			isVogal = true;
		} else {
			isVogal = false;
		}
		return isVogal;
	}

	private static String obterLetra() {
		System.out.println("Informe a letra:");
		String letra = sc.nextLine();
		return letra;
	}

}
